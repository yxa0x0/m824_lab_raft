package kvraft

import (
	"crypto/rand"
	"math/big"
	"sync"
	"time"

	"../labrpc"
)

type Clerk struct {
	servers []*labrpc.ClientEnd
	// You will have to modify this struct.
	mu       sync.Mutex
	lastSucc int
	ID       int64
}

func nrand() int64 {
	max := big.NewInt(int64(1) << 62)
	bigx, _ := rand.Int(rand.Reader, max)
	x := bigx.Int64()
	return x
}

func MakeClerk(servers []*labrpc.ClientEnd) *Clerk {
	ck := new(Clerk)
	ck.servers = servers
	// You'll have to add code here.
	ck.lastSucc = 0
	ck.ID = nrand()

	return ck
}

//
// fetch the current value for a key.
// returns "" if the key does not exist.
// keeps trying forever in the face of all other errors.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.Get", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//

func (ck *Clerk) Get(key string, cli int) string {

	// You will have to modify this function.
	DPrintf("%v: cli enter Get", cli)
	ck.mu.Lock()
	DPrintf("%v: cli enter Get with lock hold", cli)

	defer ck.mu.Unlock()
	args := GetArgs{Key: key, Cid: clientId{Id: ck.ID, OpId: time.Now().UnixNano()}}
	
	loop := true
	for loop {
		reply := GetReply{}
		if !ck.servers[ck.lastSucc].Call("KVServer.Get", &args, &reply) {
			continue
		}

		switch reply.Err {
		case "":
			DPrintf("%v: clerk Get fin, val %v", cli, reply.Value)
			return reply.Value
		case "dup request":
			args.Cid.OpId = time.Now().UnixNano()
		// try another server
		case "server is not leader":
		fallthrough
		case "commit fail":
			ck.lastSucc = (ck.lastSucc + 1) % len(ck.servers)
		// below is execute error
		default:
			DPrintf("%v: clerk get Err %v from %v", cli, reply.Err, ck.lastSucc)
			loop = false
		}
	}
	//key is not exist
	return ""
}

//
// shared by Put and Append.
//
// you can send an RPC with code like this:
// ok := ck.servers[i].Call("KVServer.PutAppend", &args, &reply)
//
// the types of args and reply (including whether they are pointers)
// must match the declared types of the RPC handler function's
// arguments. and reply must be passed as a pointer.
//
func (ck *Clerk) PutAppend(key string, value string, op string, cli int) {
	// You will have to modify this function.
	DPrintf("%v: enter PutAppend", cli)
	ck.mu.Lock()
	DPrintf("%v: enter PutAppend with lock holds", cli)
	defer ck.mu.Unlock()
	args := PutAppendArgs{Key: key, Value: value, Op: op, Cid: clientId{Id: ck.ID, OpId: time.Now().UnixNano()}}
	
	loop := true
	for loop {
		reply := PutAppendReply{}
		DPrintf("%v: clerk try send PutAppend %+v from %v", cli, args, ck.lastSucc)
		if !ck.servers[ck.lastSucc].Call("KVServer.PutAppend", &args, &reply) {
			DPrintf("%v: clerk send fail", cli)
			ck.lastSucc = (ck.lastSucc + 1) % len(ck.servers)
			continue
		}

		switch reply.Err {
		case "":
			DPrintf("%v: clerk PutAppend fin", cli)
			return
		case "dup request":
			args.Cid.OpId = time.Now().UnixNano()
		
		case "server is not leader":
			fallthrough
		case "commit fail":
			ck.lastSucc = (ck.lastSucc + 1) % len(ck.servers)
		
		// below is execute error
		default:
			DPrintf("%v: clerk appendPut Err %v from %v", cli, reply.Err, ck.lastSucc)
			loop = false
		}
	}
}

func (ck *Clerk) Put(key string, value string, cli int) {
	ck.PutAppend(key, value, "Put", cli)
}
func (ck *Clerk) Append(key string, value string, cli int) {
	ck.PutAppend(key, value, "Append", cli)
}
