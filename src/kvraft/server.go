package kvraft

import (
	"log"
	"sync"
	"sync/atomic"
	"time"

	"../labgob"
	"../labrpc"
	"../raft"
	"bytes"
)

const Debug = 1

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}

type opType = uint32

const (
	GetOp opType = iota
	PutOp
	AppendOp
)

type Op struct {
	// Your definitions here.
	// Field names must start with capital letters,
	// otherwise RPC will break.
	Key   string
	Value string
	Type  opType
	Cid   clientId
}

type resultType struct {
	opId int64
	res  string
	err  Err
}

type KVServer struct {
	mu      sync.Mutex
	me      int
	rf      *raft.Raft
	applyCh chan raft.ApplyMsg
	dead    int32 // set by Kill()

	maxraftstate int // snapshot if log grows this big

	// Your definitions here.
	data     map[string]string
	latest   sync.Map
	persister *raft.Persister
	lastExecutedIndex uint64
}

func (kv *KVServer) startExecutor() {
	go func() {
		for atomic.LoadInt32(&kv.dead) == 0 {
			msg := <-kv.applyCh
			atomic.StoreUint64(&kv.lastExecutedIndex, uint64(msg.CommandIndex))
			op := msg.Command.(Op)
			kv.execute(op)
			kv.trySnapShot()
		}
	}()
}

func (kv *KVServer) execute(op Op) {
	DPrintf("kv %v execute %+v", kv.me, op)
	ret := ""
	err := ""
	res := resultType{}
	v, ok := kv.latest.Load(op.Cid.Id)

	if !ok {
		res.opId = op.Cid.OpId
	} else {
		latestRes := v.(resultType)
		if op.Cid.OpId > latestRes.opId {
			res.opId = op.Cid.OpId
		} else {
			//dup
			return
		}
	}

	// if !ok || op.Id.OpId > latestOpId {

	// } else if op.Type != GetOp {
	// 	//dup
	// 	return latestRes.res, latestRes.err
	// }

	switch op.Type {
	case PutOp:
		kv.data[op.Key] = op.Value
	case AppendOp:
		value, exist := kv.data[op.Key]
		if !exist {
			value = ""
		}
		kv.data[op.Key] = value + op.Value
	case GetOp:
		value, exist := kv.data[op.Key]
		if !exist {
			err = "GetOp Err: key is not exist"
			break
		}
		ret = value
	default:
		panic("unknown op type!")
	}

	res.res = ret
	res.err = Err(err)
	kv.latest.Store(op.Cid.Id, res)
	DPrintf("%v exec %+v fin, store %v:%+v", kv.me, res, op.Cid.Id, res)
	//return ret, Err(err)
}

func (kv *KVServer) raftSubmit(item Op) Err {
	// kv.mu.Lock()
	DPrintf("kv %v enter raftSubmit", kv.me)
	_, _, isLeader := kv.rf.Start(item)
	// kv.mu.Unlock()
	if !isLeader {
		return "server is not leader"
	}
	// failCv = kv.rf.KvFailCv(uint32(term), uint32(index))
	// if failCv == nil {
	// 	err = "server commit fail"
	// 	return index, term, failCv, err
	// }
	return ""
}

func (kv *KVServer) getResult(Cid int64, opId int64) (string, Err) {
	for true {
		v, ok := kv.latest.Load(Cid)
		DPrintf("%v getResult Cid %v %+v ok %v", kv.me, Cid, v, ok)
		if ok {
			res := v.(resultType)
			if res.opId > opId {
				//dup
				DPrintf("%v getResult dup,  Cid %v opId %v", kv.me, opId, res.opId)
				return "", "dup request"
			}
			if res.opId == opId {
				DPrintf("%v getResult Cid %v %+v", kv.me, Cid, res)
				return res.res, res.err
			}
		}
		DPrintf("%v getResult Cid %v sleep", kv.me, Cid)
		time.Sleep(time.Duration(100) * time.Microsecond)
		// kv.commitCv.L.Lock()
		// kv.commitCv.Wait()
		// kv.commitCv.L.Unlock()
		_, isLeader := kv.rf.GetState()
		if !isLeader {
			return "", "commit fail"
		}
	}
	return "", "unknown err"
}

func (kv *KVServer) checkId(Cid int64, opId int64) (bool, resultType) {
	if v, ok := kv.latest.Load(Cid); ok {
		result := v.(resultType)
		return result.opId < opId, result
	}
	return true, resultType{}
}

func (kv *KVServer) Get(args *GetArgs, reply *GetReply) {
	// Your code here.
	DPrintf("Kv %v Get %+v\n", kv.me, args)
	kv.mu.Lock()
	defer kv.mu.Unlock()
	if exist, res := kv.checkId(args.Cid.Id, args.Cid.OpId); !exist {
		//dup
		if res.opId == args.Cid.OpId {
			reply.Value, reply.Err = res.res, res.err
		} else {
			reply.Err = "dup request"
		}
		return
	}
	item := Op{Key: args.Key, Type: GetOp, Cid: args.Cid}
	reply.Err = kv.raftSubmit(item)
	if reply.Err != "" {
		return
	}

	reply.Value, reply.Err = kv.getResult(args.Cid.Id, args.Cid.OpId)
}

func (kv *KVServer) trySnapShot() {
	if kv.maxraftstate != -1 && kv.maxraftstate < kv.persister.RaftStateSize() {
		kv.mu.Lock()
		defer kv.mu.Unlock()
		kv.makeSnapShot()
	}
}

func (kv *KVServer) makeSnapShot() {
	w := new(bytes.Buffer)
	e := labgob.NewEncoder(w)
	e.Encode(kv.lastExecutedIndex)
	e.Encode(kv.data)
	
	kv.latest.Range(func(key, value interface{}) bool {
		
		return true
	})
	data := w.Bytes()
}

// func (kv *KVServer) waitRaftCommit(index int, term int, failCv *sync.Cond) (bool, raft.ApplyMsg) {
// 	DPrintf("kv %v wait raft commit", kv.me)
// 	res := false
// 	msg := raft.ApplyMsg{}
// 	mu := sync.Mutex{}
// 	cv := sync.NewCond(&mu)
// 	mu.Lock()
// 	defer mu.Unlock()
// 	go func() {
// 		msg = <- kv.applyCh
// 		DPrintf("%v waitRaftCommit: recv %+v, index %v", kv.me, msg, index)
// 		if index == msg.CommandIndex {
// 			cv.L.Lock()
// 			res = true
// 			cv.Signal()
// 			cv.L.Unlock()
// 		} else {
// 			panic("cmd index err")
// 		}
// 	}()
// 	go func() {
// 		failCv.L.Lock()
// 		failCv.Wait()
// 		failCv.L.Unlock()
// 		cv.L.Lock()
// 		cv.Signal()
// 		cv.L.Unlock()
// 	}()

// 	cv.Wait()

// 	DPrintf("kv %v waitRaftCommit: awake, res %v", kv.me, res)
// 	return res, msg
// }

func str2OpType(str string) opType {
	switch str {
	case "Put":
		return PutOp
	case "Append":
		return AppendOp
	default:
		panic("unknown opType")
	}
}

func (kv *KVServer) PutAppend(args *PutAppendArgs, reply *PutAppendReply) {
	// Your code here.
	DPrintf("Kv %v PutAppend %+v\n", kv.me, args)

	kv.mu.Lock()
	defer kv.mu.Unlock()

	if exist, res := kv.checkId(args.Cid.Id, args.Cid.OpId); !exist {
		//dup
		if res.opId == args.Cid.OpId {
			reply.Err = res.err
		} else {
			reply.Err = "dup request"
		}
		return
	}

	item := Op{Key: args.Key, Value: args.Value, Type: str2OpType(args.Op), Cid: args.Cid}
	reply.Err = kv.raftSubmit(item)
	if reply.Err != "" {
		return
	}

	_, reply.Err = kv.getResult(args.Cid.Id, args.Cid.OpId)
}

//
// the tester calls Kill() when a KVServer instance won't
// be needed again. for your convenience, we supply
// code to set rf.dead (without needing a lock),
// and a killed() method to test rf.dead in
// long-running loops. you can also add your own
// code to Kill(). you're not required to do anything
// about this, but it may be convenient (for example)
// to suppress debug output from a Kill()ed instance.
//
func (kv *KVServer) Kill() {
	atomic.StoreInt32(&kv.dead, 1)
	kv.rf.Kill()
	// Your code here, if desired.
}

func (kv *KVServer) killed() bool {
	z := atomic.LoadInt32(&kv.dead)
	return z == 1
}

//
// servers[] contains the ports of the set of
// servers that will cooperate via Raft to
// form the fault-tolerant key/value service.
// me is the index of the current server in servers[].
// the k/v server should store snapshots through the underlying Raft
// implementation, which should call persister.SaveStateAndSnapshot() to
// atomically save the Raft state along with the snapshot.
// the k/v server should snapshot when Raft's saved state exceeds maxraftstate bytes,
// in order to allow Raft to garbage-collect its log. if maxraftstate is -1,
// you don't need to snapshot.
// StartKVServer() must return quickly, so it should start goroutines
// for any long-running work.
//
func StartKVServer(servers []*labrpc.ClientEnd, me int, persister *raft.Persister, maxraftstate int) *KVServer {
	// call labgob.Register on structures you want
	// Go's RPC library to marshall/unmarshall.
	labgob.Register(Op{})

	kv := new(KVServer)
	kv.me = me
	kv.maxraftstate = maxraftstate

	// You may need initialization code here.
	kv.data = make(map[string]string)

	kv.applyCh = make(chan raft.ApplyMsg)
	kv.rf = raft.Make(servers, me, persister, kv.applyCh)
	//kv.rf.AddStateChangeCv(kv.commitCv)
	//kv.latest = NewRWMap()

	// You may need initialization code here.
	kv.persister = persister
	kv.startExecutor()
	return kv
}
