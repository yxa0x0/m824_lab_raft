package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"log"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
	"fmt"
	"../labrpc"
)

import "bytes"
import "../labgob"


func min(a uint32, b uint32) uint32 {
	if a < b {
		return a
	}
	return b
}

const debug = 0

//var Log = log.New(f(os.OpenFile("raft.log", os.O_RDONLY, 666)), "", log.LstdFlags)
func Xlp(format string, v ... interface{}) {
	if (debug > 0) {
		log.Printf(format, v...)
	}
}

//
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
// in Lab 3 you'll want to send other kinds of messages (e.g.,
// snapshots) on the applyCh; at that point you can add fields to
// ApplyMsg, but set CommandValid to false for these other uses.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

//
// A Go object implementing a single Raft peer.
//

type RaftState = uint32

const (
	Follower RaftState = iota
	Candidate
	Master
)

const (
	HbMsMin    = 200
	HbMsMax    = 350
	ElectMsMin = 200
	ElectMsMax = 350
)

type votes struct {
	VoteId uint32
	VoteTerm uint32
}

func assert(condition bool, format string, v ... interface{}) {
	if (!condition) {
		panic(fmt.Sprintf(format, v...))
	}
}

type raftContext struct {
	currentTerm uint32
	state RaftState
}

// all immutable except Send
type logEntry struct {
	Term uint32
	Index uint32
	Log interface{}
}

func (lhs *logEntry) upToDate(rhs *logEntry) bool {
	return lhs.Term > rhs.Term || lhs.Term == rhs.Term && lhs.Index >= rhs.Index
}

type LogStruct struct {
	Entry logEntry
	Send uint32
}

type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        uint32                 // this peer's index into peers[]
	dead      int32               // set by Kill()

	// Your data here (2A, 2B, 2C).
	// Look at the paper's Figure 2 for a description of what
	// state a Raft server must maintain.

	ctx raftContext
	follower     *sync.Cond
	lastHB       time.Time
	lastElectron time.Time
	votedFor     votes
	candidate    *sync.Cond
	master       *sync.Cond
	applyMsg     *sync.Cond

	commitIndex uint32
	lastApplied uint32
	nextIndex []uint32
	matchIndex []uint32
	applyCh *chan ApplyMsg
	logs []*LogStruct
	workerCnt sync.WaitGroup
}

// for debug
func (ctx raftContext) String() string {
	return fmt.Sprintf("%v:%v", ctx.currentTerm, ctx.state)
}

func (rf *Raft) String() string {
	return fmt.Sprintf("%v:%v:%v:%v", rf.ctx.currentTerm, rf.me, rf.ctx.state, rf.commitIndex)
}

func (entry logEntry) String() string {
	return fmt.Sprintf("%v:%v:%v", entry.Term, entry.Index, entry.Log)
}

func (ctx raftContext) equal(rhs raftContext) bool {
	return ctx.currentTerm == rhs.currentTerm && ctx.state == rhs.state
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	var term int
	var isleader bool
	// Your code here (2A).
	rf.mu.Lock()
	defer rf.mu.Unlock()
	if rf.ctx.state == Master {
		isleader = true
	} else {
		isleader = false
	}

	term = int(rf.ctx.currentTerm)

	return term, isleader
}

//
// save Raft's persistent state to stable storage,
// where it can later be retrieved after a crash and restart.
// see paper's Figure 2 for a description of what should be persistent.
//
func (rf *Raft) persist() {
	// Your code here (2C).
	// Example:
	// w := new(bytes.Buffer)
	// e := labgob.NewEncoder(w)
	// e.Encode(rf.xxx)
	// e.Encode(rf.yyy)
	// data := w.Bytes()
	// rf.persister.SaveRaftState(data)
	w := new(bytes.Buffer)
	e := labgob.NewEncoder(w)
	e.Encode(rf.ctx.currentTerm)
	l := uint32(len(rf.logs))
	e.Encode(l)
	for i := 0; i < len(rf.logs); i++ {
		e.Encode(rf.logs[i].Entry)
	}
	e.Encode(rf.votedFor)
	data := w.Bytes()
	rf.persister.SaveRaftState(data)
}

//
// restore previously persisted state.
//
func (rf *Raft) readPersist(data []byte) bool {
	if data == nil || len(data) < 1 { // bootstrap without any state?
		return false
	}
	// Your code here (2C).
	// Example:
	// r := bytes.NewBuffer(data)
	// d := labgob.NewDecoder(r)
	// var xxx
	// var yyy
	// if d.Decode(&xxx) != nil ||
	//    d.Decode(&yyy) != nil {
	//   error...
	// } else {
	//   rf.xxx = xxx
	//   rf.yyy = yyy
	// }
	r := bytes.NewBuffer(data)
	d := labgob.NewDecoder(r)
	// rf.mu.Lock()
	// defer rf.mu.Unlock()
	var term uint32
	var v votes
	var l uint32
	if d.Decode(&term) != nil ||
	   d.Decode(&l) != nil {
		return false
	}
	logs := make([]*LogStruct, l)
	for i := uint32(0); i < l; i++ {
		Entry := logEntry{}
		if d.Decode(&Entry) != nil {
			return false
		}
		logs[i] = &LogStruct{Entry: Entry}
	}
	if d.Decode(&v) != nil {
		return false
	}
	rf.ctx.currentTerm = term
	rf.logs = logs
	rf.votedFor = v
	return true
}

//
// example RequestVote RPC arguments structure.
// field names must start with capital letters!
//
type RequestVoteArgs struct {
	// Your data here (2A, 2B).
	Term        uint32
	CandidateId uint32
	LastLogIndex uint32
	LastLogTerm uint32
}

//
// example RequestVote RPC reply structure.
// field names must start with capital letters!
//
type RequestVoteReply struct {
	// Your data here (2A).
	Term    uint32
	Granted bool
}


//
// example RequestVote RPC handler.
//
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {
	// Your code here (2A, 2B).

	rf.mu.Lock()
	defer rf.mu.Unlock()
	reply.Term = rf.ctx.currentTerm
	Xlp("%v called Rvote from %+v", rf, args)


	vote := func() {
		last := &rf.logs[len(rf.logs) - 1].Entry

		Xlp("%v: %+v cmp %+v", rf, last, args)
		if args.Term == rf.ctx.currentTerm && rf.votedFor.VoteTerm < args.Term && 
		   (&logEntry{Term: args.LastLogTerm, Index: args.LastLogIndex}).upToDate(last) {
			rf.votedFor.VoteId = args.CandidateId
			rf.votedFor.VoteTerm = args.Term
			rf.persist()
			reply.Granted = true
			Xlp("%v vote!", rf)
			rf.lastHB = time.Now()
		} else {
			Xlp("%v Dont vote!", rf)
			reply.Granted = false
		}

	}

	switch rf.ctx.state {
	case Follower:
		if args.Term > rf.ctx.currentTerm {
			rf.ctx.currentTerm = args.Term
			rf.ToFollower()
			vote()
			if rf.follower != nil {
				rf.follower.Signal()
			}
		}
	case Candidate:
		if args.Term > rf.ctx.currentTerm {
			rf.ctx.currentTerm = args.Term
			vote()
			rf.ToFollower()
			if rf.candidate != nil {
				rf.candidate.Signal()
			}
		}
	case Master:
		if args.Term > rf.ctx.currentTerm {
			rf.ctx.currentTerm = args.Term
			vote()
			rf.ToFollower()
			if rf.master != nil {
				rf.master.Signal()
			}
		}
	}
}

//
// example code to send a RequestVote RPC to a server.
// server is the index of the target server in rf.peers[].
// expects RPC arguments in args.
// fills in *reply with RPC reply, so caller should
// pass &reply.
// the types of the args and reply passed to Call() must be
// the same as the types of the arguments declared in the
// handler function (including whether they are pointers).
//
// The labrpc package simulates a lossy network, in which servers
// may be unreachable, and in which requests and replies may be lost.
// Call() sends a request and waits for a reply. If a reply arrives
// within a timeout interval, Call() returns true; otherwise
// Call() returns false. Thus Call() may not return for a while.
// A false return can be caused by a dead server, a live server that
// can't be reached, a lost request, or a lost reply.
//
// Call() is guaranteed to return (perhaps after a delay) *except* if the
// handler function on the server side does not return.  Thus there
// is no need to implement your own timeouts around Call().
//
// look at the comments in ../labrpc/labrpc.go for more details.
//
// if you're having trouble getting RPC to work, check that you've
// capitalized all field names in structs passed over RPC, and
// that the caller passes the address of the reply struct with &, not
// the struct itself.
//
func (rf *Raft) sendRequestVote(server int, args *RequestVoteArgs, reply *RequestVoteReply) bool {
	ok := rf.peers[server].Call("Raft.RequestVote", args, reply)
	return ok
}

// appendEntries

type AppendEntriesArgs struct {
	Term     uint32
	LeaderId uint32
	PrevLogIndex uint32
	PrevLogTerm uint32
	Entries []logEntry
	LeaderCommit uint32
	Heartbeat bool
}

type AppendEntriesReply struct {
	Term    uint32
	Success bool
}

func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	reply.Term = rf.ctx.currentTerm
	updTerm := func(newer bool, cv *sync.Cond) bool {
		if (!newer && args.Term >= rf.ctx.currentTerm) || (newer && args.Term > rf.ctx.currentTerm) {
			Xlp("%v meet AppE from term %v:%v, to Follower", rf, args.Term, args.LeaderId)
			// to Follower
			rf.ctx.currentTerm = args.Term
			rf.persist()
			rf.ToFollower()
			if cv != nil {
				cv.Signal()
			}
			return true
		}
		return false
	}
	
	folReply := func() {
		if !args.Heartbeat {
			prev := 0
			found := false

			if args.PrevLogIndex <= rf.commitIndex {
				for len(args.Entries) > 0 {
					if args.Entries[0].Index <= rf.commitIndex {
						args.Entries = args.Entries[1:]
					} else {
						break
					}
				}
				for ; prev < len(rf.logs) - 1; prev++ {
					if rf.logs[prev + 1].Entry.Index > rf.commitIndex {
						break
					}
				}
				found = true
			} else {
				for ; prev < len(rf.logs); prev++ {
					if args.PrevLogTerm == rf.logs[prev].Entry.Term && args.PrevLogIndex == rf.logs[prev].Entry.Index {
						found = true
						break
					}
				}
			}
			
			s := fmt.Sprintf("appendE %+v found %v:%v in %v, logs:", args, found, prev, rf)
			// for i := 0; i < len(rf.logs); i++ {
			// 	s += fmt.Sprintf("%+v", *rf.logs[i])
			// }
			Xlp(s)
			if found {
				reply.Success = true
				rf.logs = rf.logs[:prev + 1]
				for i := 0; i < len(args.Entries); i++ {
					e := LogStruct{Entry: args.Entries[i], Send: uint32(0)}
					rf.logs = append(rf.logs, &e)
					Xlp("%v add entry %v", rf, e)
				}
				rf.persist()
				go rf.updateCommit(min(args.LeaderCommit, rf.logs[len(rf.logs) - 1].Entry.Index))
			}
		} else {
			// heartbeat
			go rf.updateCommit(min(args.LeaderCommit, rf.logs[len(rf.logs) - 1].Entry.Index))
		}
	}

	switch rf.ctx.state {
	case Master:
		if updTerm(true, nil) {
			folReply()
		}
	case Candidate:
		if updTerm(false, rf.candidate) {
			folReply()
		}
	case Follower:
		if args.Term < rf.ctx.currentTerm {
			break
		}
		rf.lastHB = time.Now()
		updTerm(true, rf.follower)
		folReply()
	}
}

//
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
func (rf *Raft) Start(command interface{}) (int, int, bool) {
	// index := -1
	// term := -1
	//isLeader := true

	// Your code here (2B).
	rf.mu.Lock()
	defer rf.mu.Unlock()

	index := int(rf.logs[len(rf.logs) - 1].Entry.Index + 1)
	term := int(rf.ctx.currentTerm)

	if rf.ctx.state != Master {
		return index, term, false
	}

	entry := &LogStruct{Entry: logEntry{Term: rf.ctx.currentTerm, Index: uint32(index), Log: command}, Send: uint32(0)}
	Xlp("%v new entry %+v", rf, *entry)
	rf.logs = append(rf.logs, entry)
	rf.persist()
	//send appendE
	if rf.master != nil {
		rf.master.Signal()
	}
	return index, term, true
}

//
// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
//
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.
	rf.mu.Lock()
	if rf.follower != nil {
		rf.follower.Signal()
	}

	if rf.candidate != nil {
		rf.candidate.Signal()
	}
	
	if rf.master != nil {
		rf.master.Signal()
	}

	rf.mu.Unlock()

	rf.workerCnt.Wait()
}

func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	return z == 1
}

func waitNotify(waitMs int, cond *sync.Cond) {
	go func() {
		time.Sleep(time.Duration(waitMs) * time.Millisecond)
		cond.L.Lock()
		cond.Signal()
		cond.L.Unlock()
	}()
}


func(rf *Raft) updateCommit(newCommitIndex uint32) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	if rf.commitIndex >= newCommitIndex {
		return
	}
	var commit *logEntry = nil
	for i := 0; i < len(rf.logs); i++ {
		if rf.logs[i].Entry.Index == newCommitIndex {
			commit = &rf.logs[i].Entry
			break
		}
	}
	if commit == nil || commit.Term != rf.ctx.currentTerm {
		return
	}
	Xlp("%v update CommitIndex %v", rf, newCommitIndex)
	rf.commitIndex = newCommitIndex
	rf.applyMsg.Signal()
}

func (rf *Raft) commmitEntry() *logEntry {
	for i := 0; i < len(rf.logs); i++ {
		if rf.logs[i].Entry.Index == rf.commitIndex {
			return &rf.logs[i].Entry
		}
	}
	assert(true, "can't find commitIndex in %v", rf)
	return &rf.logs[0].Entry
}

func (rf *Raft) ToMaster() {
	rf.ctx.state = Master
	go rf.Master(rf.ctx)
}

func (rf *Raft) Master(ctx raftContext) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	if !ctx.equal(rf.ctx) {
		return
	}

	rf.workerCnt.Add(1)
	defer rf.workerCnt.Done()
	Xlp("%v -> Master", rf)
	rf.matchIndex = make([]uint32, len(rf.peers))
	rf.nextIndex = make([]uint32, len(rf.peers))
	total := len(rf.peers)

	for i := 0; i < total; i++ {
		rf.nextIndex[i] = rf.commitIndex + 1
		rf.matchIndex[i] = 0
	}

	for i := 0; i < len(rf.logs); i++ {
		rf.logs[i].Send = 0
	}

	type sendStatus struct {
		run uint32
		retry uint32
	}
	
	rf.master = sync.NewCond(&rf.mu)
	sends := make([]sendStatus, total)
	for true {
		if atomic.LoadInt32(&rf.dead) > 0 {
			for i := 0; i < len(sends); i++ {
				atomic.StoreUint32(&sends[i].run, 1)
			}
			break
		}

		if !ctx.equal(rf.ctx) {
			// to Follower
			Xlp("mst %v@%v ctx neq %v", rf.me, ctx, rf.ctx)
			for i := 0; i < len(sends); i++ {
				atomic.StoreUint32(&sends[i].run, 1)
			}
			break
		}

		for i := 0; i < total; i++ {
			if i == int(rf.me) {
				continue
			}
			peer := rf.peers[i]
			args := AppendEntriesArgs{Term: rf.ctx.currentTerm, LeaderId: rf.me, LeaderCommit: rf.commitIndex, Heartbeat: true}
			reply := AppendEntriesReply{}
			pIndex := i
			go func() {
				// rf.mu.Lock()
				// if rf.ctx.currentTerm == args.Term && rf.ctx.state == Master {
					Xlp("%v:%v:%v -> %v AppendEntries", args.Term, args.LeaderId, args.LeaderCommit, pIndex)
					peer.Call("Raft.AppendEntries", &args, &reply)
				// }
				// rf.mu.Unlock()
			}()

			if rf.matchIndex[i] < rf.logs[len(rf.logs) - 1].Entry.Index && atomic.LoadUint32(&sends[i].run) == 0 {
				running := &sends[i].run
				retry := &sends[i].retry
				nextIndex := &rf.nextIndex[i]
				matchIndex := &rf.matchIndex[i]
				//lastCommit := rf.commitIndex

				atomic.StoreUint32(running, 1)
				entries := make([]*LogStruct, 0)
				args := AppendEntriesArgs{Term: rf.ctx.currentTerm, LeaderId: rf.me}
				for i := 0; i < len(rf.logs); i++ {
					if rf.logs[i].Entry.Index >= *nextIndex {
						entries = append(entries, rf.logs[i])
					}
					if rf.logs[i].Entry.Index == *nextIndex {
						args.PrevLogIndex = rf.logs[i - 1].Entry.Index
						args.PrevLogTerm = rf.logs[i - 1].Entry.Term
					}
				}

				if len(entries) == 0 {
					l := rf.commmitEntry()
					args.PrevLogIndex = l.Index
					args.PrevLogTerm = l.Term
				}

				//	assert(len(entries) > 0, "len of entries = 0, %v nextIndex %v", index, *nextIndex)
				go func() {
					for atomic.LoadUint32(running) > 0 {
						rf.mu.Lock()
						if !ctx.equal(rf.ctx) {
							rf.mu.Unlock()
							break
						}
						args.LeaderCommit = rf.commitIndex
						rf.mu.Unlock()
						args.Entries = make([]logEntry, 0)
						reply := AppendEntriesReply{}
						for i := 0; i < len(entries); i++ {
							args.Entries = append(args.Entries, entries[i].Entry) //immutable except Send, so maybe ok?
						}
						if !peer.Call("Raft.AppendEntries", &args, &reply) {
							break
						}
						rf.mu.Lock()
						if !ctx.equal(rf.ctx) {
							rf.mu.Unlock()
							break
						}
						if !reply.Success {
							if reply.Term > rf.ctx.currentTerm {
								rf.ctx.currentTerm = reply.Term
								rf.persist()
								rf.ToFollower()
								if rf.master != nil {
									rf.master.Signal()
								}
								rf.mu.Unlock()
								break
							}
							index := 0
							for i := 0; i < len(rf.logs); i++ {
								if rf.logs[i].Entry.Term == args.PrevLogTerm && rf.logs[i].Entry.Index == args.PrevLogIndex {
									assert(i != 0, "index fail, %v AppE reply from %v, %+v", rf, pIndex, args)
									index = i
									// *nextIndex = args.PrevLogIndex
									// args.PrevLogIndex = rf.logs[i - 1].Entry.Index
									// args.PrevLogTerm = rf.logs[i - 1].Entry.Term
									// entries = append([]*LogStruct{rf.logs[i]}, entries...)
									break
								}
							}
							(*retry)++
							var prev int

							if *retry >= 0 {
								prev = index / 2
							} else if *retry > 5 {
								prev = 0
							}
							Xlp("AppE from %v not succ, retry %v, new prev %v, index: %v, appE %+v", pIndex, *retry, prev, index, args)							
							*nextIndex = rf.logs[prev + 1].Entry.Index
							for i := index; i > prev; i-- {
								entries = append([]*LogStruct{rf.logs[i]}, entries...)
							}
							args.PrevLogIndex = rf.logs[prev].Entry.Index
							args.PrevLogTerm = rf.logs[prev].Entry.Term
							
							rf.mu.Unlock()
						} else {
							*retry = 0
							var lastIndex uint32
							if len(entries) > 0 {
								lastIndex = entries[len(entries) - 1].Entry.Index
							} else {
								lastIndex = args.PrevLogIndex
							}
							*nextIndex = lastIndex + 1
							*matchIndex = lastIndex
							Xlp("%v new nextIndex %v, matchIndex %v", pIndex, *nextIndex, *matchIndex)
							var newCommit uint32 = 0
							for i := 0; i < len(entries); i++ {
								if entries[i].Entry.Index <= rf.commitIndex || entries[i].Entry.Term < rf.ctx.currentTerm {
									continue
								}
								Xlp("%v cnt %v:%v", rf, i, atomic.LoadUint32(&entries[i].Send))
								if (1 + atomic.AddUint32(&entries[i].Send, 1)) * 2 > uint32(total) {
									newCommit = entries[i].Entry.Index
								} else if newCommit != 0 {
									break
								}
							}
							rf.mu.Unlock()
							rf.updateCommit(newCommit)
							break
						}
					}
					atomic.StoreUint32(running, 0)
				} ()
			}
		}

		waitNotify(HbMsMin, rf.master)
		rf.master.Wait()
	}
}

func (rf *Raft) ToCandidate() {
	rf.ctx.state = Candidate
	go rf.Candidate(rf.ctx)
}

func (rf *Raft) Candidate(ctx raftContext) {
	rf.mu.Lock()
	//assert(rf.ctx.state == Candidate, "%v is not Candidate", rf)
	if !ctx.equal(rf.ctx) {
		return
	}

	rf.workerCnt.Add(1)
	defer rf.workerCnt.Done()

	rf.ctx.currentTerm++
	ctx = rf.ctx
	rf.persist()

	Xlp("%v -> into Candidate", rf)
	rf.candidate = sync.NewCond(&rf.mu)
	rf.lastElectron = time.Now()
	total := len(rf.peers)
	cv := rf.candidate
	me := rf.me
	getVotes := uint64(1)

	var lastLog *logEntry = &rf.logs[len(rf.logs) - 1].Entry

	for i := 0; i < total; i++ {
		index := i
		peer := rf.peers[i]
		args := RequestVoteArgs{Term: rf.ctx.currentTerm, CandidateId: rf.me,
								LastLogIndex: (*lastLog).Index, LastLogTerm: (*lastLog).Term}
		if i == int(rf.me) {
			continue
		}
		go func() {
			reply := RequestVoteReply{}
			Xlp("%v:%v -> %v RequestVote", args.Term, me, index)
			if !peer.Call("Raft.RequestVote", &args, &reply) {
				return
			}
			//should check reply.term?
			if reply.Term > args.Term {
				rf.mu.Lock()
				if reply.Term > rf.ctx.currentTerm {
					Xlp("Candidate: %v receive RV reply from %v:%v", rf, reply.Term, index)
					rf.ctx.currentTerm = reply.Term
					rf.persist()
					rf.ToFollower()
					cv.Signal()
				}
				rf.mu.Unlock()
			}
			Xlp("RV: ->%v %+v %+v", index, args, reply)
			if reply.Granted {
				atomic.AddUint64(&getVotes, 1)
				Xlp("candidate %v get vote %v, ctx: %+v", me, atomic.LoadUint64(&getVotes), ctx)
				if atomic.LoadUint64(&getVotes) * 2 > uint64(total) {
					// to Master
					//atomic.StoreUint32(&rf.ctx.state, Master)
					rf.mu.Lock()
					// important!
					if rf.ctx.equal(ctx) {
						rf.ToMaster()
					}
					cv.Signal()
					rf.mu.Unlock()
				}
			}
		}()
	}

	waitNotify(rand.Intn(ElectMsMax-ElectMsMin)+ElectMsMin, cv)
	loop := true
	for loop {
		//log.Print(rf.lastElectron, time.Now())
		switch {
		// to Follower or Master
		case atomic.LoadInt32(&rf.dead) > 0:
			loop = false
		case !ctx.equal(rf.ctx):
			Xlp("candi %v@%v ctx neq %v", rf.me, ctx, rf.ctx)
			loop = false
		case rf.lastElectron.Add(time.Duration(ElectMsMax) * time.Millisecond).Before(time.Now()):
			// new Electron
			//time.Sleep(100 * time.Second)
			Xlp("candi %v@%v timeout, new Candidate", rf.me, ctx)
			rf.ToCandidate()
			loop = false
		default:
			waitNotify(rand.Intn(ElectMsMax-ElectMsMin)+ElectMsMin, cv)
			rf.candidate.Wait()
		}
	}
	Xlp("%v@%v out Candidate", rf.me, ctx)
	rf.mu.Unlock()
}

func (rf *Raft) ToFollower() {
	rf.ctx.state = Follower
	go rf.Follower(rf.ctx)
}

func (rf *Raft) Follower(ctx raftContext) {
	rf.mu.Lock()
	defer rf.mu.Unlock()
//	assert(rf.ctx.state == Follower, "%v is not Follower", rf)
	if !ctx.equal(rf.ctx) {
		return
	}

	rf.workerCnt.Add(1)
	defer rf.workerCnt.Done()

	Xlp("%v into Follower", rf)
	timer := func() {
		rf.follower = sync.NewCond(&rf.mu)
		timeout := rand.Intn(HbMsMax-HbMsMin) + HbMsMin
		waitNotify(timeout, rf.follower)
		rf.follower.Wait()
	}
	timer()
	loop := true

	for loop {
		switch {
		case atomic.LoadInt32(&rf.dead) > 0:
			loop = false
		
		case !ctx.equal(rf.ctx):
			Xlp("fol %v@%v ctx neq %v", rf.me, ctx, rf.ctx)
			loop = false

		case rf.lastHB.Add(time.Duration(HbMsMax) * time.Millisecond).Before(time.Now()):
			//timeout
			Xlp("fol %v timeout", rf)
			rf.ToCandidate()
			loop = false
		default:
			Xlp("fol %v receive AppendE", rf)
			timer()
		}
	}
	Xlp("%v out Follower", rf)
}

func (rf *Raft) ApplyMsg() {
	go func() {
		for true {
			rf.mu.Lock()
			for rf.commitIndex <= rf.lastApplied {
				rf.applyMsg.Wait()
			}
			msgs := make([]ApplyMsg, 0)
			for i := 0; i < len(rf.logs); i++ {
				if rf.logs[i].Entry.Index > rf.lastApplied && rf.logs[i].Entry.Index <= rf.commitIndex {
					msg := ApplyMsg{CommandValid: true, CommandIndex: int(rf.logs[i].Entry.Index), Command: rf.logs[i].Entry.Log}
					msgs = append(msgs, msg)
				}
			}
			rf.lastApplied = rf.commitIndex
			rf.mu.Unlock()
			for i := 0; i < len(msgs); i++ {
				Xlp("%v ready output %v:%v", rf, msgs[i].CommandIndex, msgs[i].Command)
				(*rf.applyCh) <- msgs[i]
			}
		}
	} ()
}

func (rf *Raft) Discard(index int) bool {
	rf.mu.Lock()
	defer rf.mu.Unlock()
	if uint32(index) > rf.commitIndex {
		return false
	}
	found := -1
	for i := 0; i < len(rf.logs); i++ {
		if rf.logs[i].Entry.Index == uint32(index) {
			found = i
			break
		}
	}
	if found == -1 {
		return false
	}
	
	rf.logs = append([]*LogStruct{rf.logs[0]}, rf.logs[found+1:]...)
	return true
}

//
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. persister is a place for this server to
// save its persistent state, and also initially holds the most
// recent saved state, if any. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = uint32(me)

	// Your initialization code here (2A, 2B, 2C).
	rf.applyCh = &applyCh
	rf.applyMsg = sync.NewCond(&rf.mu)
	rf.ApplyMsg()

	if !rf.readPersist(persister.ReadRaftState()) {
		rf.logs = make([]*LogStruct, 1)
		rf.logs[0] = &LogStruct{Entry: logEntry{Log: "NULL"}}
		rf.ctx.currentTerm = 0
	}
	rf.ToFollower()

	// go func() {
	// 	for atomic.LoadInt32(&rf.dead) == 0 {
	// 		m := <- applyCh
	// 		rf.Discard(m.CommandIndex)
	// 	}
	// }()

	// // initialize from state persisted before a crash
	// rf.readPersist(persister.ReadRaftState())

	return rf
}
