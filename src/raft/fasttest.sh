#!/usr/bin/bash

mkdir -p logs
total=100

if [ "$1" == "" ]; then echo "no test name!"; exit 1; fi

if [ "$2" != "" ]; then
    total=$2
fi

failexit=1
if [ x"$e" = x ]; then
    failexit=0
fi

succ=0

for i in $(seq 1 $total)
do
    echo "run $i / $total test"
    file="logs/`date +%H%M%y`.log"
    go test -run $1 > $file
    if [ "$?" != 0 ]
    then
        fail=`expr $i - $succ`
        echo "test $i fail!, log is $file"
        echo "succ $succ/$i(`expr $succ \* 100 / $i`%), fail $fail/$i(`expr $fail \* 100 / $i`%)"
        if [ "$failexit" != 0 ]; then
            exit 1
        fi
    else
        succ=`expr $succ + 1`
        fail=`expr $i - $succ`
        echo "test $i succ!, log is $file"
        echo "succ $succ/$i(`expr $succ \* 100 / $i`%), fail $fail/$i(`expr $fail \* 100 / $i`%)"
    fi
done