const child_process = require('child_process')
const { promisify } = require('util')
const fs = require('fs')
const path = require('path')


class taskQueue
{
	constructor(max = 5) {
		this.max = max
		this.que = []
		this.run = 0
	}
	addTask(asyncTask) {
		this.que.push(asyncTask)
	}
	startNew() {
		while (this.que.length > 0 && this.run < this.max) {
			this.run++
			(this.que.shift())().then((ret) => {
				this.run--
				this.startNew()
				return ret
			})
		}
	}
	async waitFinish() {
		await Promise.all(this.que)
	}
}

async function main(testname, {total = 100, failExit = false, max = 4, logPath = 'logs'} = {}) {
	let succ = 0
	let done = 0

	try {fs.mkdirSync(logPath)} catch(e) {}
	const q = new taskQueue(max)
    const asyncTask = (async () => {
        const p = child_process.spawn('/usr/bin/go', ['test', '-run', testname], {cwd: '/home/yxaa/6.824/src/raft'})
        const {stdout} = p
		let [code, data] = await Promise.all(
			[new Promise(res => {
				p.on('close', code => res(code))
			}),
			new Promise((res) => {
				let buf = []
				stdout.on('data', (data) => buf.push(data)).on('end', () => {
				const ret = '' + Buffer.concat(buf)
				res(ret)
			})
		})])
		done++
		if (code != 0) {
			const fn = path.join(logPath, `fail-${testname}-${new Date().valueOf()}.log`)
			fs.writeFileSync(fn, data)
			data = null
			const fail = done - succ
			console.log(`${done}/${total} succ ${succ}(${succ * 100 / done}%) fail ${fail}(${fail * 100 / done}%) test fail!\nfile in ${fn}`)
			if (failExit) {
				process.exit()
			}

		} else {
			data = null
			succ++
			const fail = done - succ
			console.log(`${done}/${total} succ ${succ}(${succ * 100 / done}%) fail ${fail}(${fail * 100 / done}%) test succ!`)
			return 0
		}
	})
	
	for (let i = 0; i < total; i++)
		q.addTask(asyncTask)
	q.startNew()
	await q.waitFinish()
}

main('2B', {max: 10, failExit: true, total: 100})

