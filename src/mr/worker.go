package mr

import "fmt"
import "log"
import "net/rpc"
import "hash/fnv"
import "os"
import "io/ioutil"
import "encoding/json"
//
// Map functions return a slice of KeyValue.
//
type KeyValue struct {
	Key   string
	Value string
}

//
// use ihash(key) % NReduce to choose the reduce
// task number for each KeyValue emitted by Map.
//
func ihash(key string) int {
	h := fnv.New32a()
	h.Write([]byte(key))
	return int(h.Sum32() & 0x7fffffff)
}

func runMap(task *GetTaskReply,
	mapf func(string, string) []KeyValue) ([]FreeTask, error) {
	log.Println("start map")
	bucket := make(map[uint32][]KeyValue)
	for _, fn := range task.Fn {
		file, err := os.Open(fn)
		if err != nil {
			log.Fatalf("cannot open %v", fn)
		}
		content, err := ioutil.ReadAll(file)
		if err != nil {
			log.Fatalf("cannot read %v", fn)
		}
		kva := mapf(fn, string(content))
		for i := range kva {
			item := &kva[i]
			hashKey := uint32(ihash(item.Key)) % task.Nreduce
			bucket[hashKey] = append(bucket[hashKey], *item)
		}
		file.Close()
	}
	
	newTasks := make([]FreeTask, 0)
	
	for k, v := range bucket {
		fn := fmt.Sprintf("mr-%d-%d", task.TaskId, k)
		of, _ := os.Create(fn)
		enc := json.NewEncoder(of)
		err := enc.Encode(v)
		if err != nil {
			panic("json encode err")
		}
		of.Close()
		newTasks = append(newTasks, FreeTask{Filename: []string{fn}, Step: StepReduce, TaskId: k})
	}
	log.Printf("task fin, newTask: %v\n", newTasks)
	return newTasks, nil
}


func runReduce(task *GetTaskReply,
reducef func(string, []string) string) error {
	log.Println("start reduce")
	bucket := make(map[string][]string)
	for _, fn := range task.Fn {
		file, err := os.Open(fn)
		if err != nil {
			return err
		}
		dec := json.NewDecoder(file)
		var kva []KeyValue
		err = dec.Decode(&kva)
		if err != nil {
			return err
		}
		for i := range kva {
			item := &kva[i]
			bucket[item.Key] = append(bucket[item.Key], item.Value)	
		}
		file.Close()
	}
	
	//output file
	ofile, err := os.Create(fmt.Sprintf("mr-out-%d", task.TaskId))
	defer ofile.Close()
	if err != nil {
		return err
	}
	// run reduce
	for k, v := range bucket {
		rVal := reducef(k, v)
		fmt.Fprintf(ofile, "%v %v\n", k, rVal)
	}

	return nil
}

//
// main/mrworker.go calls this function.
//
func Worker(mapf func(string, string) []KeyValue,
	reducef func(string, []string) string) {

	// Your worker implementation here.

	// uncomment to send the Example RPC to the master.
	// CallExample()
	var task GetTaskReply
	var newTasks []FreeTask
	var fake int
	for true {
		newTasks = make([]FreeTask, 0)
		task = GetTaskReply{}
		call("Master.GetTask", &fake, &task)
		log.Println(task)
		if task.Nreduce == 0 {
			break
		}
		var err error
		switch task.Step {
		case StepMap:
			newTasks, err = runMap(&task, mapf)
		case StepReduce:
			err = runReduce(&task, reducef)
		default:
			panic(fmt.Sprintf("unknown step value: %v", task.Step))
		}

		if err != nil {
			panic(fmt.Sprintf("run err %v", err))
		}
		
		call("Master.TaskFin", TaskFinArgs{WorkerId: task.WorkerId, NewTasks: newTasks}, &fake)
		log.Println("task submit")
	}
}

//
// example function to show how to make an RPC call to the master.
//
// the RPC argument and reply types are defined in rpc.go.
//
func CallExample() {

	// declare an argument structure.
	args := ExampleArgs{}

	// fill in the argument(s).
	args.X = 99

	// declare a reply structure.
	reply := ExampleReply{}

	// send the RPC request, wait for the reply.
	call("Master.Example", &args, &reply)

	// reply.Y should be 100.
	fmt.Printf("reply.Y %v\n", reply.Y)
}

//
// send an RPC request to the master, wait for the response.
// usually returns true.
// returns false if something goes wrong.
//
func call(rpcname string, args interface{}, reply interface{}) bool {
	// c, err := rpc.DialHTTP("tcp", "127.0.0.1"+":1234")
	sockname := masterSock()
	c, err := rpc.DialHTTP("unix", sockname)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer c.Close()

	err = c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
