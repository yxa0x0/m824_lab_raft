package mr

//
// RPC definitions.
//
// remember to capitalize all names.
//

import "os"
import "strconv"

//
// example to show how to declare the arguments
// and reply for an RPC.
//

type ExampleArgs struct {
	X int
}

type ExampleReply struct {
	Y int
}

const (
	StepMap int = iota
	StepReduce
)

type FreeTask struct {
	Filename []string
	Step int
	TaskId uint32
}

type GetTaskReply struct {
	TaskId uint32
	WorkerId uint32
	Fn []string
	Step int
	Nreduce uint32
}

type TaskFinArgs struct {
	WorkerId uint32
	NewTasks []FreeTask
}
// Add your RPC definitions here.


// Cook up a unique-ish UNIX-domain socket name
// in /var/tmp, for the master.
// Can't use the current directory since
// Athena AFS doesn't support UNIX-domain sockets.
func masterSock() string {
	s := "/var/tmp/824-mr-"
	s += strconv.Itoa(os.Getuid())
	return s
}

func assert(condition bool, errStr string) {
	if (!condition) {
		panic(errStr)
	}
}