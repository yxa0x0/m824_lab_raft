package mr

import "log"
import "net"
import "os"
import "net/rpc"
import "net/http"
import "sync"
import "container/list"
import "sync/atomic"

type inTask struct {
	fn []string
	step int
	workerId uint32
	taskId uint32
	lastDispatch int64
}

type inTaskMap map[uint32]inTask

type taskPool struct {
	freeTasks *list.List
	inTasks inTaskMap
	finishTasks *list.List
	mtx sync.Mutex
}

func newTaskPool() * taskPool {
	return &taskPool{freeTasks: list.New(), finishTasks: list.New(), inTasks: make(inTaskMap)}
}

func (p * taskPool) Done() bool {
	p.mtx.Lock()
	defer p.mtx.Unlock()
	return len(p.inTasks) == 0 && p.freeTasks.Len() == 0
}

func (p * taskPool) GetTask(workerId uint32) *FreeTask {
	p.mtx.Lock()
	defer p.mtx.Unlock()
	item := p.freeTasks.Front()
	if (item == nil) {
		return nil
	}
	p.freeTasks.Remove(item)
	ret, _ := item.Value.(FreeTask)
	p.inTasks[workerId] = inTask{taskId: ret.TaskId, workerId: workerId, fn: ret.Filename, step: ret.Step}
	return &ret
}

func (p * taskPool) AddTask(tasks []FreeTask) {
	p.mtx.Lock()
	defer p.mtx.Unlock()
	for _, t := range tasks {
		p.freeTasks.PushBack(t)
	}
}

func (p * taskPool) Lock() {
	p.mtx.Lock()
}

func (p * taskPool) Unlock() {
	p.mtx.Unlock()
}

func (p * taskPool) TaskFin(w uint32) bool {
	p.mtx.Lock()
	defer p.mtx.Unlock()
	task, ok := p.inTasks[w]
	if !ok {
		return false
	}
	delete(p.inTasks, w)
	p.finishTasks.PushBack(task)
	return true
}

type TaskPool interface {
	GetTask(uint32) *FreeTask
	AddTask([]FreeTask)
	Done() bool
	TaskFin(uint32) bool
	Lock()
	Unlock()
}

type reducePool struct {
	freeTasks map[uint32]FreeTask
	inTasks inTaskMap
	finTasks *list.List
	mtx sync.Mutex
}

func newRuducePool() * reducePool {
	return &reducePool{freeTasks: make(map[uint32]FreeTask), inTasks: make(inTaskMap), finTasks: list.New()}
}

func (p * reducePool) Lock() {
	p.mtx.Lock()
}

func (p * reducePool) Unlock() {
	p.mtx.Unlock()
}

func (p * reducePool) GetTask(workerId uint32) *FreeTask {
	p.Lock()
	defer p.Unlock()
	if len(p.freeTasks) > 10 {
		panic("reducePool size err")
	}
	var ret * FreeTask = nil
	var K uint32
	for k, v := range p.freeTasks {
		ret = &v
		K = k
		break
	}
	if ret == nil {
		log.Printf("empty freeTask, in:%v\n", len(p.inTasks))
		return ret
	}
	delete(p.freeTasks, K)
	p.inTasks[workerId] = inTask{taskId: ret.TaskId, workerId: workerId, fn: ret.Filename, step: ret.Step}
	return ret
}

func (p * reducePool) AddTask(tasks []FreeTask) {
	p.Lock()
	defer p.Unlock()
	for _, t := range tasks {
		task, ok := p.freeTasks[t.TaskId]
		if !ok {
			p.freeTasks[t.TaskId] = t
		} else {
			// merge filename
			for _, fn := range t.Filename {
				task.Filename = append(task.Filename, fn)
			}
			p.freeTasks[t.TaskId] = task
		}
		log.Printf("afteradd, k:%v, v:%v\n", t.TaskId, p.freeTasks[t.TaskId])
	}
}

func (p * reducePool) Done() bool {
	p.Lock()
	defer p.Unlock()
	return len(p.freeTasks) == 0 && len(p.inTasks) == 0
}

func (p * reducePool) TaskFin(workerId uint32) bool {
	p.Lock()
	defer p.Unlock()
	task, ok := p.inTasks[workerId]
	if !ok {
		return false
	}
	delete(p.inTasks, workerId)
	p.finTasks.PushBack(task)
	return true
}

type Master struct {
	// Your definitions here.
	mapTasks TaskPool
	reduceTasks TaskPool
	idCnt uint32
	nReduce uint32
}

func NewMaster(nReduce uint32, mapTasks []FreeTask) *Master {
	for _, t := range mapTasks {
		assert(t.Step == StepMap, "mapTasks step invaild")
	}
	mPool := newTaskPool()
	mPool.AddTask(mapTasks)
	m := &Master{mapTasks: mPool, reduceTasks: newRuducePool(), nReduce: nReduce}
	return m
}

// Your code here -- RPC handlers for the worker to call.

//
// an example RPC handler.
//
// the RPC argument and reply types are defined in rpc.go.
//
func (m *Master) Example(args *ExampleArgs, reply *ExampleReply) error {
	reply.Y = args.X + 1
	return nil
}

func (m *Master) GetTask(_ * int, reply * GetTaskReply) error {
	workerId := m.newWorkerId()
	task := m.mapTasks.GetTask(workerId)
	if task == nil {
		//wait mapTasks done
		for !m.mapTasks.Done() {
		}

		task = m.reduceTasks.GetTask(workerId)
		if task == nil {
			*reply = GetTaskReply{}
			return nil
		}
	}
	*reply = GetTaskReply{TaskId: task.TaskId, WorkerId: workerId, Fn: task.Filename, Step: task.Step, Nreduce: m.nReduce}
	log.Println(reply)
	return nil
}

func (m *Master) TaskFin(args * TaskFinArgs, reply * int) error {
	log.Printf("worker %v fin\n", args.WorkerId)
	log.Printf("newTask %v\n", args.NewTasks)
	exist := false
	if m.mapTasks.TaskFin(args.WorkerId) {
		exist = true
	} else if m.reduceTasks.TaskFin(args.WorkerId) {
		exist = true
	}
	if !exist {
		return nil
	}

	m.AddReduceTask(args.NewTasks)
	return nil
}

func (m * Master) newWorkerId() uint32 {
	ret := atomic.LoadUint32(&m.idCnt)
	atomic.AddUint32(&m.idCnt, 1)
	return ret
}

func (m * Master) AddReduceTask(tsks []FreeTask) * Master {
	for _, t := range tsks {
		assert(t.Step == StepReduce, "reduceTask invaild step")
	}
	m.reduceTasks.AddTask(tsks)
	return m
}

//
// start a thread that listens for RPCs from worker.go
//
func (m *Master) server() {
	rpc.Register(m)
	rpc.HandleHTTP()
	//l, e := net.Listen("tcp", ":1234")
	sockname := masterSock()
	os.Remove(sockname)
	l, e := net.Listen("unix", sockname)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)
}

//
// main/mrmaster.go calls Done() periodically to find out
// if the entire job has finished.
//
func (m *Master) Done() bool {
	return m.mapTasks.Done() && m.reduceTasks.Done()
}

//
// create a Master.
// main/mrmaster.go calls this function.
// nReduce is the number of reduce tasks to use.
//
func MakeMaster(files []string, nReduce int) *Master {

	mTasks := make([]FreeTask, len(files))
	for i, f := range files {
		mTasks[i] = FreeTask{Step: StepMap, Filename: []string{f}, TaskId: uint32(i)}
	}
	m := NewMaster(uint32(nReduce), mTasks)
	//m.AddTask(FreeTask{Step: StepMap, Filename: files, TaskId: 0})
	// Your code here.
	m.server()
	log.Println("server master init finish")
	return m
}
